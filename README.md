# Proyecto del Curso Práctico de React JS⚛️

## Platzi Video (Version AnimeVideoPlatzi)

Instruido por:

Oscar Barajas Tavares

Descripción:

En este curso tomaremos el frontend construido en el Curso de Frontend de la Escuela de JavaScript y lo implementaremos todo llevandolo a componentes de React para agregar interactividad a PlatziVideo ((Version AnimeVideoPlatzi))

(https://www.youtube.com/watch?v=oELFNug8drU)

Puede ver el proyecto aquí: (https://animevideoplatzi-jorge-vicuna.vercel.app/) App

[![AnimeVideoPlatzi](https://i.imgur.com/tMXonSE.png)](https://www.youtube.com/watch?v=oELFNug8drU)
[![AnimeVideoPlatzi](https://i.imgur.com/bvbxaT8.png)](https://www.youtube.com/watch?v=oELFNug8drU)
[![AnimeVideoPlatzi](https://i.imgur.com/ayXZXH7.png)](https://www.youtube.com/watch?v=oELFNug8drU)

## Nota 🏁 (Consiguraciones):

inicializamos:

`npm init -y`

### Comandos Practico Reactjs:

- `npm install react react-dom`
- `npm i @babel/core @babel/preset-env babel-loader @babel/preset-react --save-dev`
- `npm i webpack webpack-cli html-webpack-plugin html-loader --save-dev`
- `npm install webpack-dev-server --save-dev`
- `npm install --save-dev mini-css-extract-plugin css-loader sass sass-loader`
- `npm install --save-dev eslint babel-eslint eslint-config-airbnb eslint-plugin-import eslint-plugin-react eslint-plugin-jsx-a11y`
- `npm install --save-dev file-loader`
- `npm install -g json-server`
- `npm install prop-types`

Para el comando de sass debes instalar antes : `npm install -g sass` y luego `npm install --save-dev sass`
uso `saas` en lugar de `node-sass` para que no exitsa problemas de incompatilidad con node, ya que he visto muchos casos.

### Comandos seccion React-router y redux:

- `npm install react-router-dom`
- `npm install redux react-redux`
- `npm install md5`
- `npm install classnames`

### Optimizacion Anime:

- `npm install react-player` pude usar el Player.jsx(lo use con videos de anime, pero como debian de ser del formato mp4 y no tenia ni una subida en internet, solo localmente, preferi reemplazarlo por el archivo ModalVideo.jsx y usar React-Player para usar videos de Youtube)

- `npm install @emotion/react @emotion/styled`
- `npm install react-spinners` Estos use para loading y requiere del comando anterior,

### Modificaciones en scripts del archivo .babelrc :

```
{
    "presets": [
      "@babel/preset-env",
      "@babel/preset-react"
    ],
    "plugins": ["@emotion"]
  }
```

### Modificaciones en scripts del archivo package.json :

```
  "scripts": {
    "test": "echo \"Error: no test specified\" && exit 1",
    "build": "webpack --mode=production",
    "start": "webpack-dev-server --open --mode=development", //Corre el proyecto
    "fakeapp": "npx json-server --watch initialState.json", //Corre la FakeApi
    "start2proyects": "json-server initialState.json & webpack serve --mode development" //Corre la FakeApi y el proyecto,
    // el fakeapi ya no es usado en la seccion Router
  },
```

### Creamos el archivo .eslintrc :

- Link del Gist: `https://gist.github.com/gndx/60ae8b1807263e3a55f790ed17c4c57a`

### Creamos el archivo .gitignore :

- Link del Gist: `https://gist.github.com/gndx/747a8913d12e96ff8374e2125efde544`

## Para usar Vercel App (DEPLOY A VERCEL Actualización) 🚩:

(https://animevideoplatzi-jorge-vicuna.vercel.app/) App

#### 1.-Intro

- 1.1.- Instalar de forma global vercel:
  - `npm install -g vercel`
- 1.2.- Necesitamos registrarnos en vercel.com
- 1.3.- Debemos loguearnos desde nuestra consola, con:

  - `vercel login`

  Escogen loguearse con su email(o su cuenta de github,gitlab,etc) y enter

#### 2.- Deploy de nuestro proyecto principal:

- 2.1.- Creamos el archivo vercel.json en la raiz del proyecto.
- 2.2.- copiamos la siguiente informacion a vercel.json reemplazando los valores:
  ```
  {
    "version": 2,
    "name": "animevideoplatzi-jorgevicuna",
    "builds": [
        { "use": "@vercel/static-build", "src": "package.json"}
    ],
    "routes": [
        {
            "src": "(.*).js",
            "dest": "/$1.js"
        },
        {
            "src": "(.*).json",
            "dest": "/$1.json"
        },
        {
            "src": "(.*).css",
            "dest": "/$1.css"
        },
        {
            "src": "(.*).png",
            "dest": "/$1.png"
        },
        {
            "src": "(.*).jpg",
            "dest": "/$1.jpg"
        },
        {
            "src": "/.*",
            "dest": "index.html"
        }
    ]
  }
  ```
- 3.4.- ejecutamos `vercel` en la términal y seguimos los siguientes pasos:

  ```
  ? Set up and deploy “~/projects/curso-platzi-react-avanzado/api”? [Y/n] (Seleccionamos Y, y enter).
  ? Which scope do you want to deploy to? escogemos nuestra única opcion.(debe aparecer el email con el que nos registramos, y enter).
  ? Link to existing project? [y/N] n (Seleccionamos n, y enter).
  ? What’s your project’s name? petgram-TU_NOMBRE (introducimos nuestro nombre de proyecto y enter).
  ? In which directory is your code located? ./ (aqui solo enter).
  (Como salida en la términal deberia Aparecer el link de nuestro deploy)

  Ejemplo de salida:

  🔗  Linked to jorge-vicuna/petgram-serve-jorge-vicuna (created .vercel and added it to .gitignore)
  🔍 Inspect: https://vercel.com/jorge-vicuna/petgram-serve-jorge-vicuna/5MhzSgBUfxapAav7S7dm5fuZqgTG [2s]
  ✅ Production: https://petgram-serve-jorge-vicuna.vercel.app [copied to clipboard] [35s]
  📝 Deployed to production. Run `vercel --prod` to overwrite later (https://vercel.link/2F).
  💡 To change the domain or build command, go to https://vercel.com/jorge-vicuna/petgram-serve-jorge-vicuna/settings
  ```

#### Esta documentación me ayudo:

- (https://platzi.com/tutoriales/1601-react-avanzado/11554-deploy-a-vercel-2021/)
