const path = require('path')// el modulo path sirve para trabajar con archivos y rutas de directorios,Require es la sentencia que se usa para importar un módulo y 'path' es el nombre del módulo que deseamos importar. Dicho módulo se almacena en una constante llamada "path".
const HtmlWebpackPlugin = require('html-webpack-plugin') // Estamos trayendo el módulo que se acababa de intalar desde npm, y almacenándolo en una constante.
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

module.exports = {
    entry: { // Punto de entrada
        main: path.resolve(__dirname, './src/index.js') // Entry main: sirve para indicar el archivo de Javascript de entrada de este proyecto.El ./src/index.js ya es el que viene por defecto, epro igual lo coloco como guia.
    
    },

    output: { // Punto de salida
    path: path.resolve(__dirname, './dist'), // por defecto crea en dist, puedo modificar si quiero otro nombre de carpeta
    filename: 'app.bundle.js', // Output filename: Sirve para decirle el nombre del archivo del bundle que va a generar.
    publicPath: '/', // este es el path donde encontrara el bundle, lo añadimos para evitar el el error: ERRO_ABORTED 404
    }, 
    resolve: {
        extensions: ['.js', '.jsx'],
    },
    devServer: {
      historyApiFallback: true,
    },
    module: {
        rules: [
          {
            test: /\.(js|jsx)$/,
            exclude: /node_modules/,
            use: {
              loader: 'babel-loader',
            },
          },
          {
            test: /\.html$/,
            use: {
              loader: 'html-loader',
              
            },
          },
          {
            test: /\.(s*)css$/,
            use: [
              { loader: MiniCssExtractPlugin.loader },
              'css-loader',
              'sass-loader',
            ],
          },
          {
            test: /\.(png|gif|jpg)$/,
            use: [
              {
                loader: 'file-loader',
                options: { name: 'assets/[hash].[ext]' },
              }
            ],
          }
        ],
      },
      plugins: [
        new HtmlWebpackPlugin({
          template: './public/index.html',
          filename: './index.html',
        }),

        new MiniCssExtractPlugin({
            filename: 'assets/[name].css',
          }),
      ],
}