import React from 'react'
import ReactDOM from 'react-dom'
import { Provider } from 'react-redux'
import { createStore, compose } from 'redux'
import reducer from './reducers'
import App from './routes/App'

const initialState = {
    "user": {},
    "playing": {},
    "myList": [],
    "search": [],
    "trends": [
      {
        "id": 2,
        "slug": "tvshow-2",
        "title": "Overlord",
        "type": "Accion",
        "language": "Sub-Espanol",
        "year": 2015,
        "contentRating": "16+",
        "duration": 39,
        "cover": "https://blob.animeflash.xyz/media/kNTNaiMu/medium.jpg",
        "description": "Vestibulum ac est lacinia nisi venenatis tristique",
        "source": "https://www.youtube.com/watch?v=KOWcj7XKnfQ"
      },
      {
        "id": 3,
        "slug": "tvshow-7",
        "title": "Kimetsu no Yaiba",
        "type": "Drama",
        "language": "Sub-Espanol",
        "year": 2019,
        "contentRating": "16+",
        "duration": 26,
        "cover": "https://blob.animeflash.xyz/media/dvy8CqKJ/medium.jpg",
        "description": "Vestibulum ac est lacinia nisi venenatis tristique",
        "source": "https://www.youtube.com/watch?v=2-rH7Wh7zDM"
      },
      {
        "id": 4,
        "slug": "tvshow-4",
        "title": "Shingeki no Kyojin",
        "type": "Comedy",
        "language": "Sub-Espanol",
        "year": 2013,
        "contentRating": "16+",
        "duration": 75,
        "cover": "https://blob.animeflash.xyz/media/Y0NcGJkD/medium.jpg",
        "description": "Vestibulum ac est lacinia nisi venenatis tristique",
        "source": "https://www.youtube.com/watch?v=1q2_A7y4QDw"
      },
      {
        "id": 5,
        "slug": "tvshow-5",
        "title": "Dragon Ball Super",
        "type": "Scripted",
        "language": "Sub-Espanol",
        "year": 2015,
        "contentRating": "16+",
        "duration": 131,
        "cover": "https://blob.animeflash.xyz/media/6P3HfCNR/medium.jpg",
        "description": "Vestibulum ac est lacinia nisi venenatis tristique",
        "source": "https://www.youtube.com/watch?v=j61ts42E2Ms"
      },
      {
        "id": 6,
        "slug": "tvshow-6",
        "title": "Tokyo Revengers 卍",
        "type": "Scripted",
        "language": "Sub-Espanol",
        "year": 2021,
        "contentRating": "16+",
        "duration": 24,
        "cover": "https://blob.animeflash.xyz/media/nghqMkvf/medium.jpg",
        "description": "Vestibulum ac est lacinia nisi venenatis tristique",
        "source": "https://www.youtube.com/watch?v=By_JYrhx-WY"
      },
      {
        "id": 7,
        "slug": "tvshow-7",
        "title": "Tensei shitara Slime Datta Ken",
        "type": "Drama",
        "language": "Sub-Espanol",
        "year": 2018,
        "contentRating": "16+",
        "duration": 48,
        "cover": "https://blob.animeflash.xyz/media/PbXFAGGX/medium.jpg",
        "description": "Vestibulum ac est lacinia nisi venenatis tristique",
        "source": "https://www.youtube.com/watch?v=GhGTc6p8sg0"
      }    
    ],
    "originals": [
      {
        "id": 8,
        "slug": "tvshow-2",
        "title": "One Piece",
        "type": "Accion",
        "language": "Sub-Espanol",
        "year": 1999,
        "contentRating": "16+",
        "duration": 1000,
        "cover": "https://blob.animeflash.xyz/media/ed2UKJws/medium.jpg",
        "description": "Vestibulum ac est lacinia nisi venenatis tristique",
        "source": "https://www.youtube.com/watch?v=3V8F17meuxs"
      },
      {
        "id": 9,
        "slug": "tvshow-3",
        "title": "Bleach",
        "type": "Adventure",
        "language": "Sub-Espanol",
        "year": 2004,
        "contentRating": "16+",
        "duration": 366,
        "cover": "https://blob.animeflash.xyz/media/869CJobO/medium.jpg",
        "description": "Vestibulum ac est lacinia nisi venenatis tristique",
        "source": "https://www.youtube.com/watch?v=VUgxCUd3Bsk"
      },
      {
        "id": 10,
        "slug": "tvshow-10",
        "title": "Dragon Ball Z",
        "type": "Animation",
        "language": "Sub-Espanol",
        "year": 1989,
        "contentRating": "16+",
        "duration": 519,
        "cover": "https://blob.animeflash.xyz/media/QE0buNbo/medium.jpg",
        "description": "Vestibulum ac est lacinia nisi venenatis tristique",
        "source": "https://www.youtube.com/watch?v=-r27tgxNr1I"
      },
      {
        "id": 11,
        "slug": "tvshow-11",
        "title": "Naruto Shippuden",
        "type": "War",
        "language": "Sub-Espanol",
        "year": 2017,
        "contentRating": "16+",
        "duration": 500,
        "cover": "https://blob.animeflash.xyz/media/8fOi8jwO/medium.jpg",
        "description": "Vestibulum ac est lacinia nisi venenatis tristique",
        "source": "https://www.youtube.com/watch?v=0OqZhwSRYwE"
      },
      {
        "id": 12,
        "slug": "tvshow-12",
        "title": "InuYasha",
        "type": "Comedy",
        "language": "Sub-Espanol",
        "year": 1996,
        "contentRating": "16+",
        "duration": 167,
        "cover": "https://blob.animeflash.xyz/media/d8vbw5MS/medium.jpg",
        "description": "Vestibulum ac est lacinia nisi venenatis tristique",
        "source": "https://www.youtube.com/watch?v=ji0Tb2x4eWw"
      },
      {
        "id": 13,
        "slug": "tvshow-13",
        "title": "Death Note",
        "type": "Drama",
        "language": "Sub-Espanol",
        "year": 2006,
        "contentRating": "16+",
        "duration": 37,
        "cover": "https://blob.animeflash.xyz/media/5MsMeLYw/medium.jpg",
        "description": "Vestibulum ac est lacinia nisi venenatis tristique",
        "source": "https://www.youtube.com/watch?v=8QE9cmfxx4s"
      }
    ]
}


const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const store = createStore(reducer, initialState, composeEnhancers());


ReactDOM.render(  
    <Provider store={store}>
        <App />
    </Provider>,
   document.getElementById('app'))