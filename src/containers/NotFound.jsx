import React from 'react';
import { Link } from 'react-router-dom';
import Error4 from '../assets/static/404 Error-anime.png'
import '../assets/styles/components/Notfound.scss'
import Header from '../components/Header';

const NotFound = () => (
  <>
    <Header />
    <div className='base'>
      <h1 className='base__text'>No Encontrado</h1>
      <Link to="/" style={{textDecoration:'none'}}>
       <button className='base__button'>
         <h2 >Regresa al Home</h2>
       </button>
      </Link>
      <img className='base__img' src={Error4} />
    </div>
  </>
);

export default NotFound;