import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import Header from '../components/Header';
import Search from '../components/Search';
import Categories from '../components/Categories';
import Carousel from '../components/Carousel';
import CarouselItem from '../components/CarouselItem';
import useInitialState from '../hooks/useInitialState';
import { searchRequest  } from '../actions';
import '../assets/styles/App.scss';

const Home = ({ myList, trends, originals, search, searchRequest  }) => {
  const hasSearch = search.length > 0;

  useEffect(() => { 
    if (hasSearch) searchRequest('');
  }, []);
  return (
    <>
      <Header />
      <Search isHome/>

      {hasSearch &&
        <Categories title='Resultados'>
                <Carousel>
                  {search.map((item) => 
                <CarouselItem key={item.id} {...item} />,
                )}
            </Carousel>
          </Categories>
      }


      {myList.length > 0 &&
        <Categories title="Mi Lista">
          <Carousel>
            {myList.map(item =>
              <CarouselItem 
                key={item.id} 
                {...item}
                isList
              />
            )}
          </Carousel>
        </Categories>
      }
      <Categories title="Tendencias">
        <Carousel>
          {trends.map(item =>
            <CarouselItem key={item.id} {...item} />
          )}
        </Carousel>
      </Categories>
      <Categories title="Originales de Anime Platzi Video">
        <Carousel>
          {originals.map(item =>
            <CarouselItem key={item.id} {...item} />
          )}
        </Carousel>
      </Categories>
    </>
  );
}

const mapStateToProps = state => {
  return {
    myList: state.myList,
    trends: state.trends,
    originals: state.originals,
    search: state.search,
    searchRequest: state.searchRequest,
  };
};

const mapDispatchToProps = {
  searchRequest,
};

export default connect(mapStateToProps,mapDispatchToProps)(Home);