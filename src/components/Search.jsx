import React from 'react';
import classNames from 'classnames';
import '../assets/styles/components/Search.scss';
import { connect } from 'react-redux';
import { searchRequest  } from '../actions';



const Search = ({ isHome,searchRequest }) => {
  const inputStyle = classNames('input', {
    isHome
  });

  const handleSearch = (e) => {
    searchRequest(e.target.value);
  };
  

  return (
    <section className="main">
      <h2 className="main__title">¿Qué quieres ver hoy?</h2>
      <input type="text" className={inputStyle} placeholder="Buscar..." onChange={handleSearch}/>
    </section>
  );
}

const mapDispatchToProps = {
  searchRequest ,
};

export default connect(null, mapDispatchToProps)(Search); 